The purpose is to convert a google spreadsheet to json.

The current spreadsheet id is: 1aJDWIJbc1-EvIhm-vCQjXXDMaPEIipfX4FBSBEEA3Ew
https://docs.google.com/spreadsheets/d/1aJDWIJbc1-EvIhm-vCQjXXDMaPEIipfX4FBSBEEA3Ew/edit#gid=1334710850

To make it accessible I had to make the spreadsheet public AND 'file -> publish to web'.

The spreadsheet above got the data from:
https://www.openfigi.com/?utm_medium=BSYM&utm_campaign=Figi&utm_source=Website

For Nasdaq stocks, I used UB as the exchange code. There are 4700 results.
