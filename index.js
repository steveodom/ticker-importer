var gsjson = require('google-spreadsheet-to-json');
var jsonfile = require('jsonfile');
var _ = require('lodash');
var Promise = require('bluebird');
var name = require('namer').name;
var recipe = require('./recipe.json');

function writeModelTickerEvents(recipe, options={}, cb) {
  var baseOptions = {
    spreadsheetId: '1aJDWIJbc1-EvIhm-vCQjXXDMaPEIipfX4FBSBEEA3Ew',
    worksheet: 'highVolume',
    bucket: 'trading-features',
    klass: 'stocks',
    period: '10y'
  }
  var config = Object.assign({}, baseOptions, options);
  gsjson({
      spreadsheetId: config.spreadsheetId,
      worksheet: config.worksheet
  }).then(function(result) {
    return Promise.resolve(_.map(result, 'ticker'));

  })
  .then(function(tickers) {
    return Promise.resolve(_.chunk(tickers, 20));
  })
  .then(function(array) {
    var id = name('stocks').id;
    return Promise.map(array, (tickers, i) => {
      var base = {
        "tickers": tickers,
        "period": config.period,
        "bucket": config.bucket,
        "id": id,
        "klass": config.klass,
        "purpose": "forecast"
      };
      return Promise.resolve(Object.assign({}, base, {recipe: recipe}));
    });
  })
  .then(function(res) {
    return Promise.resolve(res);
  })
  .catch(function(err) {
    Promise.reject(err);
  });
}

exports.writeModelTickerEvents = writeModelTickerEvents;
